import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import {SocketProvider, socketConnect} from 'socket.io-react';
import io from 'socket.io-client';
// import store from './store/AppStore';
import {Router, Route, hashHistory} from 'react-router';

import Landing from './components/Landing';
import Queue from './components/Queue';
import Room from './components/Room';
// import Project from './components/Project';
// import Notes from './components/Notes';

class App extends React.Component {
    componentDidMount() {
        console.log("Main did mount");
    }

    render() {

        return (

            <Router history={hashHistory}>
                <Route path="/" component={Landing}/>
                <Route path="/queue" component={Queue}/>
                <Route path="/room/:id/:sid" component={Room}/>
            </Router>
        )
    }
}

ReactDOM.render(
    <App></App>, document.getElementById('app'));
