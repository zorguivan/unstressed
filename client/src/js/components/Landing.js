import React from 'react';
import {Link} from 'react-router';
import io from 'socket.io-client';

export default class Landing extends React.Component {
    render() {
        return (
            <div>
                <div class="row">
                    <div class="col-md-5">
                        <h3>Unstressed Listernes Tips:</h3>
                    </div>
                </div>
                <br></br>
                <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-10">

                        Three words define the great Listener: unconditional positive regard. People come to Unstressed for help.<br/><br/>
                        <ul>
                            <li>Though it is true that sometimes people need “tough love” and I have been in the place of giving that tough love, all advice given should always be with unconditional positive regard for the Venter, they are a human being and they can improve themselves if they set themselves to the task, most of them are here because either they do not believe in themselves or because nobody has believed in them.</li><br/>
                            <li>As Listeners, it is on us to believe in the ability of the Venter to make themselves better, to take them at their word, give them the benefit of the doubt, and to show them that it is entirely possible for someone to believe in them.</li><br/>
                            <li>Negative regard achieves absolutely nothing.</li><br/>
                            <li>It is impossible to shame or degrade a person into improving.</li><br/>
                            <li>Negative regard does nothing more than communicate negativity, and the Venter likely already feels badly enough about themselves.</li><br/>
                            <li>Some Venters’ issue IS that they have no faith or positive regard for themselves!</li>
                        </ul>
                    </div>
                </div>
                <div class="alert alert-info disclaimer-box" role="alert">
                    <strong>Disclaimer :
                    </strong><br/>
                    Users providing this Unstressed are regular people with
                    <strong> no professional training</strong>
                    behind their advice. Venting to a stranger
                    <strong>can be incredibly dangerous if you are at a very mentally sensitive state</strong>. By entering the chat rooms, you understand that Unstressed is not responisble for any advice given or conversations conducted during a chat session !!
                </div>
                <div class="row">
                    <div class="col-md-4"></div>
                    <div class="col-md-4">
                      You can
                        <Link to={"/queue"}>
                            <button type="button" className="btn btn-primary">
                                <span className="glyphicon glyphicon-comment"></span>  Listen
                            </button>
                        </Link>
                        Or you can
                        <Link to={"/queue"}>
                            <button type="button" className="btn btn-success">
                                <span className="glyphicon glyphicon-comment"></span>  Vent
                            </button>
                        </Link>

                    </div>
                    <div class="col-md-4"></div>
                </div>
            </div>
        )
    }
}
