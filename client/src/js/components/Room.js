import React from 'react';
import ReactDOM from 'react-dom';
import {Link} from 'react-router';
import Scroll from 'react-scroll';
import io from 'socket.io-client';
import ReactEmojiMixin from 'react-emoji';

export default class Room extends React.Component {
    constructor() {
        super();
        this.state = {
            messages: [],
            connection: true
        }
    }
    componentWillMount() {
        console.log(this.props.params);
    }
    componentDidMount() {
        this.socket = io('/');
        this.socket.on('message', message => {
          let audio = new Audio('sounds/notification.mp3');
            audio.play();

            let data = message.message;
            this.setState({
                messages: [
                    ...this.state.messages,
                    message
                ]
            });
            let objDiv = this.refs.messagesContainer;
            objDiv.scrollTop = (objDiv.scrollHeight + 30);
        });
        this.socket.on('disconnection', state => {
            this.setState({connection: false});
            console.log('Disconnected')
        })
        this.socket.on('typing', state => {
          this.setState({typing: true});
          console.log('Typing');
        })
        this.socket.on('not-typing', state => {
          this.setState({typing: false});
        })
    }
    mixins : [ReactEmojiMixin];



    hundleSend(event) {
      let body = event.target.value;
      if(body.length > 0){
        this.socket.emit('typing');
      } else {
        this.socket.emit('not-typing');
      }
        if (event.keyCode == 13 && body) {
            let objDiv = this.refs.messagesContainer;
            objDiv.scrollTop = (objDiv.scrollHeight + 30);
            console.log(objDiv.scrollTop, objDiv.scrollHeight);
            let message = {
                body,
                from: 'Me'
            }
            this.setState({
                messages: [
                    ...this.state.messages,
                    message
                ]
            });
            event.target.value = '';
            this.sendMessage(body)
        }
    }

    sendMessage(body) {
        let objDiv = this.refs.messagesContainer;
        objDiv.scrollTop = (objDiv.scrollHeight + 30);
        console.log(objDiv.scrollTop, objDiv.scrollHeight);
        this.socket.emit('message', {body: body});
        this.socket.emit('not-typing');
    }

    leaveRoom() {
        this.socket.emit('left');
        document.location.href = '/';
        setTimeout(function() {
            document.location.href = '/';
        }, 200);
        setTimeout(function() {
            document.location.href = '/';
        }, 0);
    }
    updateScroll() {
        let objDiv = this.refs.messagesContainer;
        if (objDiv && objDiv.scrollTop && objDiv.scrollHeight) {
            objDiv.scrollTop = objDiv.scrollHeight;
            console.log(objDiv.scrollTop, objDiv.scrollHeight);
        }
    }
    render() {
        this.updateScroll();
        console.log(this.state);
        Scroll.animateScroll.scrollToBottom();
        let messages = <div id="connectionMsg">
            <h4>You have found a stranger, Say hi !</h4>
        </div>
        let partnerState ;
        if(this.state.typing){
          partnerState = <div id="typing"><img src="images/typing.gif" width="80px"/></div>
        }
        if (this.state.messages.length > 0) {
            messages = this.state.messages.map((message, index) => {
                let msg = '';
                if (message.from == 'Me') {
                    msg = <div class="ownMsg">{ReactEmojiMixin.emojify(message.body)}</div>
                } else {
                    msg = <div class="strngrMsg">{ReactEmojiMixin.emojify(message.body)}</div>
                }
                return <div key={index} class="messageContainer">
                    {msg}
                </div>
            })
        }
        this.updateScroll();
        let Container = <div>
            <div className="messagesContainer" id="msgsContainer" ref="messagesContainer">
                {messages}
            </div>
            <div className="row formInput">
              <div className="col-md-1">{partnerState}</div>
                <div className="col-md-10"><input type="text" class="form-control input-lg" placeholder="message" onKeyUp={this.hundleSend.bind(this)}/></div>
                <div className="col-md-1">
                    <button type="button" class="btn btn-danger btn-lg" onClick={this.leaveRoom.bind(this)}>Leave
                    </button>
                </div>
            </div>
        </div>
        if (this.state && !this.state.connection) {
            Container = <div>
                <div className="messagesContainer" id="msgsContainer" ref="messagesContainer">
                    {messages}
                    <div id="leaverMsg">
                        <h3>-- The Connection Broke or your Partner has been disconnected --
                        </h3>
                    </div>
                </div>
                <div className="row formInput">
                    <div className="col-md-11"><input type="text" class="form-control input-lg" placeholder="message" onKeyUp={this.hundleSend.bind(this)} disabled/></div>
                    <div className="col-md-1">
                        <button type="button" class="btn btn-primary btn-lg" onClick={this.leaveRoom.bind(this)}>Leave
                        </button>
                    </div>
                </div>
            </div>
        }
        this.updateScroll();
        return (
            <div>
                {Container}
            </div>

        )
    }
}

Room.propTypes = {
    params: React.PropTypes.object
}
