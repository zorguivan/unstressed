import React from 'react';
import {browserHistory} from "react-router";
import io from 'socket.io-client';

export default class Queue extends React.Component {
    constructor() {
        super();
        this.state = {
            id: []
        }
    }
    componentWillMount() {
        let socket = io('/');
        socket.emit('queue');
    }
    componentDidMount() {
      this.socket = io('/');

        this.socket.on('queue', queue => {
            let id = queue.id;
            this.setState({...this.state.id, id});
        })
        this.socket.on('session', res => {
            document.location.href = '/#/room/' + res.clients[0].id + '/' + res.clients[1].id;
        })
        this.socket.on('disconnection', err => {
          console.log("Partner Disconnected");
        })
    }
    render() {

        return (
            <div>
                <div class="row">
                    <div class="col-md-4"></div>
                    <div class="col-md-4 queueLogo">
                        <img src="images/Queue.gif" width="90px"/>
                    </div>
                    <div class="col-md-4"></div>
                </div>
                <div class="row">
                    <div class="col-md-4"></div>
                    <div class="col-md-4 queueLogo">
                        <h3>Searching for chat<span class="loading"></span>
                        </h3>
                    </div>
                    <div class="col-md-3"></div>
                </div>
            </div>
        )
    }
}
