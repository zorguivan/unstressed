var express = require('express');
var http = require('http');
var socketIo = require('socket.io');
var path = require('path');
var bodyParser = require('body-parser');
var mysql = require('mysql');
var q = require('q');

var app = express();
var server = http.createServer(app);
var io = socketIo(server);

var users = [];

function addUser(user) {
    users.push(user);
    console.log('Total of ' + users.length + ' users are online')
}
function updateUser(user) {
    var user = user;
    if (users.length > 0) {
        users.forEach(function(usr) {
            if (usr.id == user.id) {
                usr.state = 'session';
            }
        });
    }
}
function deleteUser(id) {
    if (users.length > 0) {
        for (var i = 0; i < users.length; i++) {
            console.log(users);
            if (users[i].id == id) {
                users.splice(i, 1);
            }
        }
        console.log('User left : ' + id);
    }
}
io.on('connection', function(socket) {
    socket.on('queue', function(queue) {
        addUser({state: 'queue', id: socket.id, socket: socket});
        console.log('Guest-' + socket.id + ' Has been connected');
        socket.broadcast.emit('queue', {
            state: 'queue',
            id: socket.id
        });

        if (users.length > 0 && users.length % 2 === 0) {
            console.log('Chat Active');
            socket.broadcast.emit('session', {
                state: 'session',
                clients: [
                    {
                        id: users[0].id
                    }, {
                        id: users[1].id
                    }
                ]
            })
        }
    })
    socket.on('message', function(message) {
        console.log(message)
        socket.broadcast.emit('message', {
            body: message.body,
            from: socket.id
        })
    })
    socket.on('left', function() {
        console.log('Guest-' + socket.id + ' Has been disconnected');
        deleteUser(socket.id);
        socket.broadcast.emit('disconnection', {state: 'disconnected'});

    })
    socket.on('typing', function(){
      socket.broadcast.emit('typing', {state: 'typing'});
    })
    socket.on('not-typing', function(){
      socket.broadcast.emit('not-typing', {state: 'not-typing'});
    })
    socket.on('disconnect', function() {
        console.log('Guest-' + socket.id + ' Has been disconnected');
        deleteUser(socket.id);
        if (users.length > 0 && users.length % 2 != 0) {
            socket.broadcast.emit('disconnection', {state: 'disconnected'});
        }
    });
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(express.static(path.join(__dirname, 'node_modules')));
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'client')));
app.use(express.static(path.join(__dirname, 'client/bower_components')));

function removeUser(userName) {
    console.log('Removing a user ' + userName);
    var execution = q.defer();
    var query = "REMOVE FROM users WHERE name = :name";
    connection.query(query, {
        name: userName
    }, function(err, res) {
        if (err) {
            console.log(err);
            execution.reject(err);
            return;
        }
        execution.resolve(res);
    });
    return execution.promise;
}

var router = express.Router();
// app.use(router);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.json({error: err})
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.json({error: err})
});

server.listen(3001)

module.exports = app;
